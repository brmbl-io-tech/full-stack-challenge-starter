defmodule BrmblWeb.UserLiveTest do
  use BrmblWeb.ConnCase

  import Phoenix.LiveViewTest
  import Brmbl.AccountsFixtures

  @create_attrs %{}
  @update_attrs %{}
  @invalid_attrs %{}

  defp create_user(_) do
    user = user_fixture()
    %{user: user}
  end

  describe "Index" do
    setup [:create_user]

    test "lists users", %{user: user, conn: conn} do
      # FIXME
    end

    test "paginates the user list", %{user: user, conn: conn} do
      # FIXME
    end

    test "provides a search filter", %{user: user, conn: conn} do
      # FIXME
    end
  end
end
