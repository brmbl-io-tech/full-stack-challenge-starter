defmodule BrmblWeb.APIControllerTest do
  use BrmblWeb.ConnCase, async: true

  alias Brmbl.Accounts
  import Brmbl.AccountsFixtures

  setup %{conn: conn} do
    user = user_fixture()

    %{user: user, conn: conn}
  end

  describe "GET /api/top-three" do
    test "returns the top 3 api users", %{conn: conn} do
      # FIXME
    end

    test "increments the access count by 1", %{user: user, conn: conn} do
      # FIXME
    end
  end
end
