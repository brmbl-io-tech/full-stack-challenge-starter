# Brmbl

This is a starter repo for the Bramble Coding Challenge. It takes care of the basic setup, and in some cases (not all) provides hints, indicated by FIXME comments.

## Getting Started

First, please create your own repository (DO NOT FORK THIS ONE) - push it to GitLab, and share it with us, working as you would in a professional environment.

Please don't forget to update this README to reflect how to build and run your application.

## Development

### Setup

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup` (this will create a test user)
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

This user was created as part of the seed
```
test-user@test.com
12345678
```
