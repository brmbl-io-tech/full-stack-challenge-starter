defmodule Brmbl.Repo do
  use Ecto.Repo,
    otp_app: :brmbl,
    adapter: Ecto.Adapters.Postgres
end
