defmodule BrmblWeb.PageController do
  use BrmblWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
