defmodule BrmblWeb.UserLive.Index do
  use BrmblWeb, :live_view

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(_params, _url, socket) do
    {:noreply, assign(socket, page_title: "User List")}
  end
end
